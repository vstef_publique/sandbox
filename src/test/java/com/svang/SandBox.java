package com.svang;

import org.json.JSONArray;
import org.json.JSONObject;

public class SandBox {

	public static void main(String[] args) {
		String json = "[{\"nom\":\"Ecole primaire Michel Servet\",\"addresse\":\"2-6 Rue Alsace Lorraine, 69001 Lyon\",\"nbEleve\":\"350\",\"status\":\"1\",\"latitude\":\"45.7720199\",\"longitude\":\"4.8368897999999945\"},{\"nom\":\"OGEC GROUPE SCOLAIRE SAINT DENIS\",\"addresse\":\"7 Rue Hénon, 69004 Lyon\",\"nbEleve\":\"100\",\"status\":\"1\",\"latitude\":\"45.779445\",\"longitude\":\"4.831833999999958\"},{\"nom\":\"Ecole Primaire Privée Saint Sacrement\",\"addresse\":\"2-6 Rue Alsace Lorraine, 69001 Lyon\",\"nbEleve\":\"250\",\"status\":\"1\",\"latitude\":\"45.7720199\",\"longitude\":\"4.8368897999999945\"},{\"nom\":\"Ecole maternelle publique Anatole France\",\"addresse\":\"15 Rue Louis, 69003 Lyon\",\"nbEleve\":\"50\",\"status\":\"1\",\"latitude\":\"45.7528455\",\"longitude\":\"4.888221499999986\"},{\"nom\":\"Ecole maternelle Condorcet\",\"addresse\":\"37 Rue Jules Massenet, 69003 Lyon\",\"nbEleve\":\"280\",\"status\":\"1\",\"latitude\":\"45.7482793\",\"longitude\":\"4.8924376999999595\"},{\"nom\":\"Ecole Maternelle Audrey Hepburn\",\"addresse\":\"8 Rue Tissot, 69009 Lyon\",\"nbEleve\":\"150\",\"status\":\"1\",\"latitude\":\"45.7766122\",\"longitude\":\"4.80246360000001\"},{\"nom\":\"École élémentaire\",\"addresse\":\"2-6 Rue Alsace Lorraine, 69001 Lyon\",\"nbEleve\":\"80\",\"status\":\"1\",\"latitude\":\"45.7488929\",\"longitude\":\"45.7488929\"}]";
		
		try {
			
			JSONArray ja= new JSONArray(json);
			
			for(int i = 0; i < ja.length(); i++) {
				JSONObject j = ja.getJSONObject(i);
				System.out.println(j.getString("nom"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
