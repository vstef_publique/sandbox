package com.svang;

import java.util.ArrayList;
import java.util.List;

public class User {

	 private final long id;
	    private final String login;
	    private final String mdp;
	    private final String status;
	    
		public User(long id, String login, String mdp, String status) {
			super();
			this.id = id;
			this.login = login;
			this.mdp = mdp;
			this.status = status;
		}
		
		public long getId() {
			return id;
		}
		public String getLogin() {
			return login;
		}
		public String getMdp() {
			return mdp;
		}
		
		public String getStatus() {
			return status;
		}

		public static List<User> getList(){
			List<User> users = new ArrayList<User>();
	    	
	    	User user = new User(1, "svang", "svang", "1");
	    	User user1 = new User(2, "ynov", "ynov", "1");
	    	User user2 = new User(3, "java", "java", "1");
	    	User user3 = new User(4, "android", "android", "1");
	    	User user4 = new User(5, "oreo", "oreo", "1");
	    	User user5 = new User(6, "lollipop", "lollipop", "1");
	    	
	    	users.add(user);
	    	users.add(user1);
	    	users.add(user2);
	    	users.add(user3);
	    	users.add(user4);
	    	users.add(user5);
	    	
	    	return users;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (id ^ (id >>> 32));
			result = prime * result + ((login == null) ? 0 : login.hashCode());
			result = prime * result + ((mdp == null) ? 0 : mdp.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			User other = (User) obj;
			if (id != other.id)
				return false;
			if (login == null) {
				if (other.login != null)
					return false;
			} else if (!login.equals(other.login))
				return false;
			if (mdp == null) {
				if (other.mdp != null)
					return false;
			} else if (!mdp.equals(other.mdp))
				return false;
			return true;
		}
		
		
}
