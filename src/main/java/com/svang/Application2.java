package com.svang;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class Application2 {

	public static void main(String[] args) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity("https://www.cdiscount.com/search/10/nintendo+switch.html", String.class);
        System.out.println(getTitle(response.getBody()));

	}
	
	public static String getTitle(String html){
		String title = html.substring(html.indexOf("<span class=\"price\">"), html.indexOf("</span>"));
		return title;
	}

}
