package com.svang;

import java.util.ArrayList;
import java.util.List;

public class Personnage{
	
	String id;
	String nom;
	String espece;
	String sexe;
	String description;
	
	static List<Personnage> lists = new ArrayList<Personnage>();
	
	public Personnage() {
		super();
		// TODO Auto-generated constructor stub
	}	
	public Personnage(String id, String nom, String espece, String sexe, String description) {
		super();
		this.id = id;
		this.nom = nom;
		this.espece = espece;
		this.sexe = sexe;
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEspece() {
		return espece;
	}

	public void setEspece(String espece) {
		this.espece = espece;
	}


	public String getSexe() {
		return sexe;
	}


	public void setSexe(String sexe) {
		this.sexe = sexe;
	}



	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public static List<Personnage> getPersonnage(){
		if(lists.size() == 0) {
			Personnage perso1 = new Personnage("1", "Songoku", "Saiyan", "M", "Son Goku, ou Sangoku, alias Goku dans les versions antérieures du manga, est le principal protagoniste du manga Dragon Ball et aussi le principal héros. Il est le petit-fils adoptif de Grand-père Son Gohan, le mari de Chichi, le père de Son Gohan et de Son Goten, le grand-père de Pan , et plus tard arrière-arrière-grand-père de Son Goku Jr. Il est le petit frère de Raditz, et fils de Bardock et de Gine.");		
			Personnage perso2 = new Personnage("2", "Vegeta", "Saiyan", "M", "Vegeta est un des personnages principaux de la saga Dragon Ball. Il est le prince de la planète Vegeta, son père est le roi Vegeta. Il est le mari de Bulma et le père de Trunks et Bra. Il est l'éternel rival de Son Goku.");
			Personnage perso3 = new Personnage("3", "Songohan", "Saiyan/Terrien", "M", "Son Gohan, ou Sangohan dans les versions antérieures du manga, est l'un des personnages de fiction créé par Akira Toriyama dans le manga Dragon Ball, en 1988. Son Gohan est le premier fils que Son Goku a avec Chichi. On peut dire que sa naissance correspond au passage de Dragon Ball à Dragon Ball Z. Son Gohan apparaît comme un petit garçon timide. Sa mère Chichi voudrait en faire un grand savant, ce qui n'est pas du goût de Son Goku, qui lui souhaiterait voir en son fils un grand combattant. Son Gohan, tout comme son père, a horreur que l'on s'attaque aux faibles. Cela provoque en lui une colère irraisonnée qui lui permet de décupler ses forces.");
			Personnage perso4 = new Personnage("4", "Trunks", "Saiyan/Terrien", "M", "Trunks est l'un des personnages principaux de Dragon Ball. Il est le fils de Vegeta et de Bulma. Par conséquent, le Roi Vegeta est son grand-père paternel, Monsieur Brief son grand-père maternel, Madame Brief sa grand-mère maternelle, Bra sa soeur et Tarble son oncle.");
			Personnage perso5 = new Personnage("5", "Piccolo", "Namek", "M", "Piccolo dit Ma Junior, ou traduit maladroitement Petit-Coeur dans la version française de l'anime, est l'un des personnages principaux de Dragon Ball. Il est le fils de Piccolo Daimaô, et aussi en quelque sorte sa réincarnation et le rival de Son Gokû. Stratégiste sage et expert qui était à l'origine un ennemi implacable de Gokû, Piccolo devient plus tard un membre permanent des Z-Fighters lors de la série Dragon Ball Z, en particulier lorsqu'il forme un lien étroit avec le fils de Gokû, Son Gohan, après l'avoir formé en préparation de l'arrivée imminente de les Saiyans et d'autres menaces futures.");
			Personnage perso6 = new Personnage("6", "Broly", "Saiyan", "M", "Broly étant le Super Saiyan Légendaire, il a un physique quelque peu différent des autres Super Saiyan. À l'état normal, il a les cheveux noirs comme les autres Saiyan, mais possède déjà un gabarit très impressionnant de par sa taille et sa musculature. Le front de Son Gokû arrive à la base de son cou.");
			Personnage perso7 = new Personnage("7", "Kid Buu", "Majin", "M", "Majin Boo originel est la véritable forme de Majin Boo. La première forme de Boo qui apparaît dans Dragon Ball Z Majin Boo reprend cette forme après que tous les combattants qu'il ait absorbé soient retirés de son organisme.");
			Personnage perso8 = new Personnage("8", "Cell", "Cyborg", "M", "Cell est l'un des trois antagonistes principaux de Dragon Ball Z avec Freezer et Boo. C'est un humain artificiel de type évolutif créé par l'ordinateur du Dr Gero. Il est un être humanoïde entièrement organique créé artificiellement, notamment par le biais de manipulations génétiques (Bio-organisme).");
			Personnage perso9 = new Personnage("9", "Freezer", "Demon du froid", "M", "Freezer est l'un des personnages majeurs de la saga Dragon Ball ainsi que l'un des trois antagonistes principaux de Dragon Ball Z avec Cell et Boo. C'est un personnage-clé de la série : de puissance nettement supérieure à celle des ennemis rencontrés jusque-là, il contraint Son Gokû à se transformer en Super Saiyan et ouvre ainsi une nouvelle ère dans la série. Chaque membre de la famille de Freezer porte un nom anglais faisant référence au froid : son père est le Roi Cold (froid), son frère se nomme Cooler (climatiseur, ou glacière), et l'un de ses ancêtres s'appelait Chilled (vient de chirudo, qui signifie « réfrigéré »).");
			Personnage perso10 = new Personnage("10", "Vegetto", "Saiyan", "M", "Vegetto, également appelé Vegeku dans la première version française de l'anime, est le résultat de la fusion de Gokû et de Vegeta grâce aux Potaras qui permettent de fusionner instantanément.");
			
			lists.add(perso1);
			lists.add(perso2);
			lists.add(perso3);
			lists.add(perso4);
			lists.add(perso5);
			lists.add(perso6);
			lists.add(perso7);
			lists.add(perso8);
			lists.add(perso9);
			lists.add(perso10);
		}
		return lists;
	}

	public static List<Personnage> getLists() {
		return lists;
	}

	public static void setLists(List<Personnage> lists) {
		Personnage.lists = lists;
	}
	
	public static void add(Personnage p) {
		Personnage.lists.add(p);
	}
	
	
	
	
}
