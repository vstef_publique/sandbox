package com.svang;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	@RequestMapping("/getEcolePrimaire")
    public List<EcolePrimaire> getEcolePrimaire() {
    	
    	List<EcolePrimaire> lists = EcolePrimaire.getEcolePrimaire();    	
    	return lists;
    }
	
	@RequestMapping("/getEntree")
    public List<Personnage> getEntree() {    	
    	List<Personnage> lists = Personnage.getPersonnage();    	
    	return lists;
    }
	
	@RequestMapping("/getEntreeById")
    public Personnage getEntree(@RequestParam(value="id") String id) {    	
    	List<Personnage> lists = Personnage.getPersonnage();    	
    	
    	for ( Personnage personnage : lists) {
			if(personnage.getId().equals(id)) {
				return personnage;
			}
		}
    	
    	return null;
    }
	
	@RequestMapping("/images/lesbonsplansdebibi/{nomImageId:.+}")
    public ResponseEntity<byte[]> getImage(@PathVariable(value="nomImageId") String nomImage) {    	
    	byte[] bFile = null;
		try {
			bFile = Files.readAllBytes(new File("src/main/resources/static/images/lesbonsplansdebibi/" + nomImage).toPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    return ResponseEntity.ok(bFile);
    }
	
	@RequestMapping(method = RequestMethod.POST, value = "/saveEntree", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<String> saveEntree(@RequestBody Personnage personnage) {    	
    	List<Personnage> lists = Personnage.getPersonnage();    
    	String id = (lists.size() + 1) + "";
    	personnage.setId(id);
    	lists.add(personnage);
    	
    	return ResponseEntity.status(HttpStatus.OK).build();
    }
	
    @RequestMapping("/authorization")
    public String greeting(@RequestParam(value="login") String login,
    								@RequestParam(value="mdp")String mdp) {
    	
    	List<User> users = User.getList();    	
    	for (User user : users) {
			if(user.getLogin().equals(login)
					&& user.getMdp().equals(mdp)) {
				return "true";
				
			}
		}
    	
    	return "false";
    }    
}
