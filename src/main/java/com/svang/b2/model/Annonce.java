package com.svang.b2.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.StringUtils;

public class Annonce implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4179267425703680954L;
	
	private Integer id;
	private String nomVendeur;
	private String email;
	private transient String mdp;
	private String titre;
	private String localisation;
	private String categorie;
	private Double prix;
	private String description;
	private String image;
	private Date dateCreation;
	
	static List<Annonce> lists = new ArrayList<Annonce>();
		
	public static List<Annonce> getAnnonce(String freeField, String categorie, String ville){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		List<Annonce> newList = new ArrayList<>();
		try {
			if(lists.size() == 0) {				
				Annonce annonce = new Annonce();
				annonce.setId(1);
				annonce.setNomVendeur("AssKicker");
				annonce.setEmail("asskicker@ynov.com");
				annonce.setMdp("asskicker");
				annonce.setTitre("A vendre Polos Lacoste toutes tailles (neuf)");
				annonce.setLocalisation("Lyon");
				annonce.setCategorie("Vêtements");
				annonce.setPrix(35D);
				annonce.setDescription("Bonjour,\nJe vends des polos Lacoste neufs.\n35 euros l'unité.\nBonne journée.");
				annonce.setImage("src/main/resources/static/images/lesbonsplansdebibi/polo.jpeg");
				annonce.setDateCreation(sdf.parse("2018-04-15 18:00"));
		
				Annonce annonce2 = new Annonce();
				annonce2.setId(2);
				annonce2.setNomVendeur("AssKicker");
				annonce2.setEmail("asskicker@ynov.com");
				annonce2.setMdp("asskicker");
				annonce2.setTitre("A vendre BMW 335i comme neuve");
				annonce2.setLocalisation("Lyon");
				annonce2.setCategorie("Voitures");
				annonce2.setPrix(25000D);
				annonce2.setDescription("Bonjour,\nJe vends mon BMW 335i 50000Km comme neuve.\nPlus d'infos, contacter moi.\nBonne journée.");
				annonce2.setImage("src/main/resources/static/images/lesbonsplansdebibi/bm.jpg");
				annonce2.setDateCreation(sdf.parse("2018-04-15 18:00"));
		
				lists.add(annonce);
				lists.add(annonce2);
			}
			///////Filtre sur titre
			if(StringUtils.isEmpty(freeField) && !StringUtils.isEmpty(categorie) && !StringUtils.isEmpty(ville)) {
				newList.addAll(lists.stream().filter(a -> a.categorie.toUpperCase().contains(categorie.toUpperCase())
						&& a.localisation.toUpperCase().contains(ville.toUpperCase())
						).collect(Collectors.toList()));
			}
						
			if(!StringUtils.isEmpty(freeField) && StringUtils.isEmpty(categorie) && StringUtils.isEmpty(ville)) {
				newList.addAll(lists.stream().filter(a -> a.titre.toUpperCase().contains(freeField.toUpperCase())).collect(Collectors.toList()));
			}
			//filtre sur cat
			if(StringUtils.isEmpty(freeField) && !StringUtils.isEmpty(categorie) && StringUtils.isEmpty(ville)) {
				newList.addAll(lists.stream().filter(a -> a.categorie.toUpperCase().contains(categorie.toUpperCase())).collect(Collectors.toList()));
			}
			
			if(!StringUtils.isEmpty(freeField) && !StringUtils.isEmpty(categorie) && StringUtils.isEmpty(ville)) {
				newList.addAll(lists.stream().filter(a -> a.categorie.toUpperCase().contains(categorie.toUpperCase())
						&& a.titre.toUpperCase().contains(freeField.toUpperCase())
						).collect(Collectors.toList()));
			}
						
			//Filtre sur ville
			if(StringUtils.isEmpty(freeField) && StringUtils.isEmpty(categorie) && !StringUtils.isEmpty(ville)) {
				newList.addAll(lists.stream().filter(a -> a.localisation.toUpperCase().contains(ville.toUpperCase())).collect(Collectors.toList()));
			}
			
			if(!StringUtils.isEmpty(freeField) && StringUtils.isEmpty(categorie) && !StringUtils.isEmpty(ville)) {
				newList.addAll(lists.stream().filter(a -> a.localisation.toUpperCase().contains(ville.toUpperCase())
						&& a.titre.toUpperCase().contains(freeField.toUpperCase())
						).collect(Collectors.toList()));
			}
			
			
			if(StringUtils.isEmpty(freeField) 
					&& StringUtils.isEmpty(categorie)
					&& StringUtils.isEmpty(ville)) {
				newList = lists;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return newList;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNomVendeur() {
		return nomVendeur;
	}
	public void setNomVendeur(String nomVendeur) {
		this.nomVendeur = nomVendeur;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getCategorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	public Double getPrix() {
		return prix;
	}
	public void setPrix(Double prix) {
		this.prix = prix;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getLocalisation() {
		return localisation;
	}

	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	
	
}
