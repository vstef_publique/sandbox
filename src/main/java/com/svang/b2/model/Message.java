package com.svang.b2.model;

import java.io.Serializable;

public class Message implements Serializable{
	private String id;
	private String nom;
	private String email;
	private String numeroTelephone;
	private String message;
	private String nomVendeur;
	private String idAnnonce;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNumeroTelephone() {
		return numeroTelephone;
	}
	public void setNumeroTelephone(String numeroTelephone) {
		this.numeroTelephone = numeroTelephone;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getNomVendeur() {
		return nomVendeur;
	}
	public void setNomVendeur(String nomVendeur) {
		this.nomVendeur = nomVendeur;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getIdAnnonce() {
		return idAnnonce;
	}
	public void setIdAnnonce(String idAnnonce) {
		this.idAnnonce = idAnnonce;
	}
	
	
}
