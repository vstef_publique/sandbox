package com.svang.b2;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.svang.b2.model.Annonce;
import com.svang.b2.model.Message;

@RestController
public class LesPetitesAnnoncesDeBibiController {
	
	@RequestMapping("/findAnnonces")
    public List<Annonce> getAnnonce(@RequestParam(value="motCle", required = false) String motCle,
    		@RequestParam(value="categorie", required = false) String categorie,
    		@RequestParam(value="localisation", required = false) String localisation) {
    	
    	List<Annonce> lists = Annonce.getAnnonce(motCle, categorie, localisation);
    	return lists;
    }
	
	@RequestMapping(method = RequestMethod.POST, value = "/sendMessage", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<String> sendMessage(@RequestBody Message message) {    
		
		if(message != null && StringUtils.isEmpty(message.getMessage())) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
    	
    	return ResponseEntity.status(HttpStatus.CREATED).build();
    }
	
	@PostMapping("/saveAnnonce")
    public ResponseEntity<String> saveAnnonce(@RequestParam("annonce")  String annonce, @RequestParam("file") MultipartFile file) {    
		
		 Annonce jsonAnnonce;
		try {
			jsonAnnonce = new ObjectMapper().readValue(annonce, Annonce.class);
			List<Annonce> lists = Annonce.getAnnonce(null, null, null);    
	    	Integer id = lists.size() + 1;
	    	jsonAnnonce.setId(id);
	    	
	    	byte[] bytes = file.getBytes();	    	
	    	String fileName =file.getOriginalFilename();
	    	String extension = fileName.substring(fileName.lastIndexOf("."));
	    	String lienImage = "src/main/resources/static/images/lesbonsplansdebibi/" + id + extension;
           
	    	Path path = Paths.get(lienImage);
            Files.write(path, bytes);
            
            jsonAnnonce.setImage(lienImage);
	    	lists.add(jsonAnnonce);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		} 
    	return ResponseEntity.status(HttpStatus.CREATED).build();
    }
	
	@DeleteMapping("/deleteAnnonce")
    public ResponseEntity<String> deleteAnnonce(@RequestParam("id")  Integer id) {    
		
		List<Annonce> lists = Annonce.getAnnonce(null, null, null); 
		
		
		for (Annonce a : lists) {
			if(a.getId().intValue() == id.intValue()) {
				lists.remove(a);
				return ResponseEntity.status(HttpStatus.OK).build();
			}
		}
		
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
	
	@PutMapping("/updateAnnonce")
    public ResponseEntity<String> updateAnnonce(@RequestBody Annonce annonce) {    
		
		List<Annonce> lists = Annonce.getAnnonce(null, null, null); 
		
		
		for (Annonce a : lists) {
			if(a.getId().intValue() == annonce.getId().intValue()) {
				lists.remove(a);
				lists.add(annonce);
				return ResponseEntity.status(HttpStatus.OK).build();
			}
		}
		
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    	
    }
	
	@GetMapping("/findAnnoncesByEmail")
    public List<Annonce> findAnnoncesByEmail(@RequestParam("email") String email, @RequestParam("mpd") String mdp) {    
		
		List<Annonce> lists = Annonce.getAnnonce(null, null, null); 
		List<Annonce> newList = new ArrayList<>();
		newList = lists.stream().filter(a -> a.getEmail().toUpperCase().equals(email.toUpperCase()) && a.getMdp().equals(mdp)).collect(Collectors.toList());
		
    	return newList;
    }

}
