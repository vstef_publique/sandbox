package com.svang;

import java.util.ArrayList;
import java.util.List;

public class EcolePrimaire {
	
	private String id;
	private String nom;
	private String addresse;
	private String nbEleve;
	private String status;
	private String latitude;
	private String longitude;
	

	public EcolePrimaire(String id, String nom, String addresse, String nbEleve, String status, String latitude, String longitude) {
		super();
		this.id = id;
		this.nom = nom;
		this.addresse = addresse;
		this.nbEleve = nbEleve;
		this.status = status;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public static List<EcolePrimaire> getEcolePrimaire(){
		List<EcolePrimaire> lists = new ArrayList<EcolePrimaire>();
		EcolePrimaire ecolePrimaire = new EcolePrimaire("1", "Ecole primaire Michel Servet", "2-6 Rue Alsace Lorraine, 69001 Lyon", "350", "1", "45.7720199", "4.8368897999999945");
		EcolePrimaire ecolePrimaire1 = new EcolePrimaire("2", "École élémentaire", "2-6 Rue Alsace Lorraine, 69001 Lyon", "80", "1", "45.7488929", "45.7488929");
		EcolePrimaire ecolePrimaire2 = new EcolePrimaire("3", "OGEC GROUPE SCOLAIRE SAINT DENIS", "7 Rue Hénon, 69004 Lyon", "100", "1", "45.779445", "4.831833999999958");
		EcolePrimaire ecolePrimaire3 = new EcolePrimaire("4", "Ecole Primaire Privée Saint Sacrement", "15 Rue Etienne Dolet, 69003 Lyon", "250", "1", "45.757568", "4.851882");
		EcolePrimaire ecolePrimaire4 = new EcolePrimaire("5", "Ecole maternelle publique Anatole France", "15 Rue Louis, 69003 Lyon", "50", "1", "45.7528455", "4.888221499999986");
		EcolePrimaire ecolePrimaire5 = new EcolePrimaire("6", "Ecole maternelle Condorcet", "37 Rue Jules Massenet, 69003 Lyon", "280", "1", "45.7482793", "4.8924376999999595");
		EcolePrimaire ecolePrimaire6 = new EcolePrimaire("7", "Ecole Maternelle Audrey Hepburn", "8 Rue Tissot, 69009 Lyon", "150", "1", "45.7766122", "4.80246360000001");
		
		lists.add(ecolePrimaire);
		lists.add(ecolePrimaire2);
		lists.add(ecolePrimaire3);
		lists.add(ecolePrimaire4);
		lists.add(ecolePrimaire5);
		lists.add(ecolePrimaire6);
		lists.add(ecolePrimaire1);
		
		return lists;
		
		
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getAddresse() {
		return addresse;
	}

	public void setAddresse(String addresse) {
		this.addresse = addresse;
	}

	public String getNbEleve() {
		return nbEleve;
	}
	public void setNbEleve(String nbEleve) {
		this.nbEleve = nbEleve;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
